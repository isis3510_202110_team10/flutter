import 'package:flutter/material.dart';

class VenueDropdown extends StatefulWidget {
  final void Function(String) onSelectedVenue;
  Future<List<dynamic>> venues;
  VenueDropdown({Key key,this.venues, @required this.onSelectedVenue}) : super(key: key);
  @override
  _VenueDropdown createState() => _VenueDropdown();
}

class _VenueDropdown extends State<VenueDropdown> {
 
  


  List<DropdownMenuItem<dynamic>> _dropdownMenuItems;
  dynamic _selectedItem;

  List<DropdownMenuItem<dynamic>> buildDropDownMenuItems(listItems) {

  List<DropdownMenuItem<dynamic>> items = [];
    for (var listItem in listItems[0]["venues"]) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem["name"]),
          value: listItem,
        ),
      );
    }
 
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return 
        FutureBuilder(future:widget.venues, builder: (context, snapshot) {
        if (snapshot.hasData) {
            _dropdownMenuItems = buildDropDownMenuItems(snapshot.data);
            _selectedItem = _dropdownMenuItems[0].value;
             
        return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: const EdgeInsets.only(left: 10.0,right: 10.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all()),
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                    value: _selectedItem,
                    items: _dropdownMenuItems,
                    onChanged: (value) {
                    
                      widget.onSelectedVenue(value["id"]);
                      setState(() {
                        _selectedItem = value;
                      });
                    }),
              ),
            ),
    );
      } else {
          return Center(child: CircularProgressIndicator());
      }});
          
  }

  

}   

class ListItem {
  String id;
  String name;

  ListItem(this.id, this.name);
}