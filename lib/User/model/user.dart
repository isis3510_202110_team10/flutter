import 'package:flutter/material.dart';
import '../../Restaurant/model/restaurant.dart';
import '../../Category/model/category.dart';

class User {
  final String id;
  final String email;
  final String createdAt;
  final String image;
  final List<Restaurant> restaurants;
  final List<Category> cateogories;

  //myFavoritePlaces
  //myPlaces

  User(
      {Key key,
      @required this.id,
      @required this.email,
      @required this.createdAt,
      @required this.image,
      this.restaurants,
      this.cateogories});
}
