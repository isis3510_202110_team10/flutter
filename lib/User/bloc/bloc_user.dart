import 'dart:async';
import 'dart:io';




import 'package:compleat/User/ui/repository/compleat_repository.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';


class UserBloc implements Bloc {
  final _compleat_repository = CompleatRepository();


  Future<List<dynamic>> getMenu(String token)=> _compleat_repository.getMenu(token);
  
  Future<List<dynamic>> getOrders(String token)=> _compleat_repository.getOders(token);

  Future<List<dynamic>> getVenues(String token)=> _compleat_repository.getVenues(token);
  signOut() {
 
  }


  @override
  void dispose() {

  }

}