import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Login extends StatelessWidget {
  final loginAction;
  final String loginError;

  const Login(this.loginAction, this.loginError);

  
  @override
  Widget build(BuildContext context) {
    Widget button = SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.only(top:20,left: 20, right: 20),
        child: ElevatedButton(
           onPressed: () {
            loginAction();
          },
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
                  return Color.fromARGB(255,236, 187, 48);
          }),
          shape: MaterialStateProperty.all<OutlinedBorder>(StadiumBorder()),
          
        ),
          child: Padding(
            padding: EdgeInsets.only(
                top: 10 , bottom: 10 ),
            child: Text(
              "Login",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700),
            ),
          ),
        ),
      ),
    );


    Widget imagen =  Container(
      child: Stack(
        children:[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/logitopy.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
          
          decoration: BoxDecoration(
              color: Colors.white,
              gradient: LinearGradient(
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  colors: [
                    Color(0xFF141414),Color(0xFF141414).withOpacity(0.7),
                    Color(0xFF141414).withOpacity(0.1),
                  ],
                  )),
        )
        
            ),
     
          Center(
            child: Column(
              children: [
                 SizedBox(height: 200),
                 Padding(padding: EdgeInsets.all(10), child:  SvgPicture.asset('assets/images/compleat.svg'))
              ,
                 SizedBox(height: 200),
                button]
            )
          )
              ]
            ),
          );
    return imagen;
  }

 
}