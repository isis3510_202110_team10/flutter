


import 'compleat_api_provider.dart';

class CompleatRepository {

  final _compleatAPI = CompleatAPIProvider();
  Future<List <dynamic>> getMenu(String token)=> _compleatAPI.fetchMenu(token);
  Future<List <dynamic>> getOders(String token)=> _compleatAPI.fetchOrders(token);
  Future<List <dynamic>> getVenues(String token)=> _compleatAPI.fetchVenues(token);
}