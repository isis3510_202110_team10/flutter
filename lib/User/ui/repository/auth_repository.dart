import 'auth0_auth_api.dart';

class AuthRepository {
  final _auth0AuthAPI = Auth0AuthAPI();
 
  Future<void> signInAuth0()=> _auth0AuthAPI.loginAction();

  logOut()=> _auth0AuthAPI.logoutAction();


}
