
import 'dart:convert';
import 'package:http/http.dart' as http;
final _rootUrl ="http://127.0.0.1:8000/api/";

class CompleatAPIProvider {

  Future<List<dynamic>> fetchMenu(String token) async {
    final response = await http.get(_rootUrl+"menu",
     headers:{"Authorization":"Bearer $token"});
     if(response.statusCode==200){
       return json.decode(response.body);
     }
     else {
  
    throw Exception('Failed to load menu');
  }
}
  Future<List<dynamic>> fetchOrders(String token) async {
     final response = await http.get(_rootUrl+"orders",
     headers:{"Authorization":"Bearer $token"});
     if(response.statusCode==200){
       return json.decode(response.body);
     }
     else {
  
    throw Exception('Failed to load menu');
  }
  }

    Future<List<dynamic>> fetchVenues(String token) async {
     final response = await http.get(_rootUrl+"restaurants",
     headers:{"Authorization":"Bearer $token"});
     if(response.statusCode==200){
       return json.decode(response.body);
     }
     else {
  
    throw Exception('Failed to load menu');
  }
  }
}