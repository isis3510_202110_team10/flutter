/// -----------------------------------
///          External Packages        
/// -----------------------------------

import 'package:flutter/material.dart';
import 'package:compleat/User/bloc/bloc_user.dart';
import 'package:compleat/app_layout.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

/// -----------------------------------
///           Auth0 Variables          
/// -----------------------------------
import "./Auth/variables.dart";
/// -----------------------------------
///           Profile Widget           
/// -----------------------------------
import "./Widgets/Profile.dart";
/// -----------------------------------
///            Login Widget           
/// -----------------------------------
import "./Widgets/Login.dart";
import 'constants.dart';
/// -----------------------------------
///                 App                
/// -----------------------------------

void main()  => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

/// -----------------------------------
///              App State            
/// -----------------------------------

class _MyAppState extends State<MyApp> {
 


  
  @override
  Widget build(BuildContext context)  {
   
    return BlocProvider(
      child: MaterialApp(
          title: 'Compleat',
           theme: ThemeData(
            primaryColor: kSecondary,
            textTheme: Theme.of(context).textTheme.apply(bodyColor: kSecondary),
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: AppLayout(),
        ),
      bloc: UserBloc());
  }

 
}