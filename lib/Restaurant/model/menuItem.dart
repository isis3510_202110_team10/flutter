import 'dart:convert';

// Bool parsing for Strings.
extension BoolParsing on String {
  bool parseBool() {
    return this.toLowerCase() == 'true';
  }
}

// Option's choices.
class Choice {
  final int id;
  final String name;
  final double extraCost;
  final int option;
  final List<String> notAvailableAt;

  Choice(
      {this.id, this.name, this.extraCost, this.option, this.notAvailableAt});

  factory Choice.fromJson(Map<String, dynamic> json) {
    var _notAvailableAt = json['not_available_at'] != null
        ? List.from(json['not_available_at'])
        : <String>[]; // Empty String list.
    return Choice(
        id: json['id'] as int,
        name: json['name'] as String,
        extraCost: json['extra_cost'] as double,
        option: json['option'] as int,
        notAvailableAt: _notAvailableAt);
  }
}

// MenuItem's options.
class Option {
  final int id;
  final String name;
  final String description;
  final List<String> notAvailableAt;
  final int minSelectedValues;
  final int maxSelectedValues;
  final List<Choice> choices;

  Option(
      {this.id,
      this.name,
      this.description,
      this.notAvailableAt,
      this.minSelectedValues,
      this.maxSelectedValues,
      this.choices});

  factory Option.fromJson(Map<String, dynamic> json) {
    var _notAvailableAt = json['not_available_at'] != null
        ? List.from(json['not_available_at'])
        : <String>[]; // Empty String list.
    var choicesJson =
        json['choices'] != null ? json['choices'] as List : <Choice>[];
    List<Choice> _choices =
        choicesJson.map((choice) => Choice.fromJson(choice)).toList();
    return Option(
        id: json['id'] as int,
        name: json['name'] as String,
        description: json['description'] as String,
        notAvailableAt: _notAvailableAt,
        minSelectedValues: json['min_selected_values'] as int,
        maxSelectedValues: json['max_selected_values'] as int,
        choices: _choices);
  }
}

class MenuItem {
  final int id;
  final String name;
  final String description;
  final String restaurant;
  final List<String> notAvailableAt;
  final double price;
  final int preparationTime;
  final bool isFeatured;
  final bool isRecommended;
  final bool isSpicy;
  final bool hasNuts;
  final bool isVegan;
  final bool isVegetarian;
  final bool isGluttenFree;
  final List<Option> options;

  MenuItem(
      {this.id,
      this.name,
      this.description,
      this.restaurant,
      this.price,
      this.notAvailableAt,
      this.preparationTime,
      this.isFeatured,
      this.isRecommended,
      this.isSpicy,
      this.hasNuts,
      this.isVegan,
      this.isVegetarian,
      this.isGluttenFree,
      this.options});

  factory MenuItem.fromJson(Map<String, dynamic> json) {
    var _notAvailableAt = json['not_available_at'] != null
        ? List.from(json['not_available_at'])
        : <String>[];
    var optionsJson =
        json['options'] != null ? json['options'] as List : <Option>[];
    List<Option> _options =
        optionsJson.map((option) => Option.fromJson(option)).toList();
    return MenuItem(
        id: json['id'] as int,
        name: json['name'] as String,
        description: json['description'] as String,
        restaurant: json['restaurant'] as String,
        notAvailableAt: _notAvailableAt,
        price: json['price'] as double,
        preparationTime: json['preparation_time'] as int,
        isFeatured: (json['is_featured'] as String).parseBool(),
        isRecommended: (json['is_recommended'] as String).parseBool(),
        isSpicy: (json['is_spicy'] as String).parseBool(),
        hasNuts: (json['has_nuts'] as String).parseBool(),
        isVegan: (json['is_vegan'] as String).parseBool(),
        isVegetarian: (json['is_vegetarian'] as String).parseBool(),
        isGluttenFree: (json['is_gluten_free'] as String).parseBool(),
        options: _options);
  }
}

class Menu {
  final int id;
  final String name;
  final String restaurant;
  final List<String> notAvailableAt;
  final List<MenuItem> menuItems;

  Menu(
      {this.id,
      this.name,
      this.restaurant,
      this.notAvailableAt,
      this.menuItems});

  factory Menu.fromJson(Map<String, dynamic> json) {
    var _notAvailableAt = json['not_available_at'] != null
        ? List.from(json['not_available_at'])
        : <String>[];
    var menuItemsJson =
        json['items'] != null ? json['items'] as List : <MenuItem>[];
    List<MenuItem> _menuItems =
        menuItemsJson.map((menuItem) => MenuItem.fromJson(menuItem)).toList();
    return Menu(
        id: json['id'] as int,
        name: json['name'] as String,
        restaurant: json['restaurant'] as String,
        notAvailableAt: _notAvailableAt,
        menuItems: _menuItems);
  }
}

// Methods

List<Menu> parseMenu(String response) {
  final parsedMenu = json.decode(response).cast<Map<String, dynamic>>();
  return parsedMenu.map<Menu>((jsonItem) => Menu.fromJson(jsonItem)).toList();
}
