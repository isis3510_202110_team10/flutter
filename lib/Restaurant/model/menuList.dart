import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'menuItem.dart';
import 'dart:async';

import 'menuItem.dart';

Future<List<Menu>> fetchMenu() async {
  final String response =
      await rootBundle.loadString('assets/platos_demo.json');
  final data = parseMenu(response);
  return data;
}

class MenuView extends StatelessWidget {
Future<List<dynamic>> futureMenu;
  MenuView(this.futureMenu);

  @override
  Widget build(BuildContext context) {
    return Padding(
      child: FutureBuilder<List<dynamic>>(
        future: futureMenu,
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? MenuList(menu: snapshot.data)
              : Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
      padding: EdgeInsets.all(10),
    );
  }
}

class MenuList extends StatelessWidget {
  final List<dynamic> menu;

  MenuList({this.menu});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: menu.length,
      itemBuilder: (context, index) {
        return Column(
          children: <Widget>[
            ...(menu[index]["items"]).map((item) {
              return Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                color: Colors.white70,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      item["name"],
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
                    ),
                    Text(item["description"]),
                    Text('${item["price"]}'),
                    Row(
                      children: [
                        Text('Disponible: '),
                        Switch(
                          value: true,
                          onChanged: (value) {},
                          activeTrackColor: Colors.orangeAccent,
                          activeColor: Colors.orange,
                        )
                      ],
                    )
                  ],
                ),
              );
            }).toList(),
          ],
        );
      },
    );
  }
}
