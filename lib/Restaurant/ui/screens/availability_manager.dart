import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:compleat/User/bloc/bloc_user.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

import '../../../side_menu.dart';
import '../../model/menuList.dart';

class AvailabilityManager extends StatefulWidget {
  final String token;
 

  AvailabilityManager({Key key, this.token}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AvailabilityManager();
  }

}

class _AvailabilityManager extends State<AvailabilityManager> {
  var menus;

  @override
  Widget build(BuildContext context) {
   
  UserBloc userBloc = BlocProvider.of<UserBloc>(context);
     return Scaffold(
      appBar: AppBar(
        title: Text("Disponibilidades"),
      ),
      drawer: SideMenu(widget.token,(){}),
      body: Container(
        child: MenuView(userBloc.getMenu(widget.token)),
      ),
     
    );
  }
}