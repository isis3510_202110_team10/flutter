import 'package:flutter/material.dart';
import 'package:compleat/User/bloc/bloc_user.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';

import '../../../side_menu.dart';
import '../../../venue_dropdown.dart';

class OrderManager extends StatefulWidget {
    final String token;

  OrderManager({Key key, this.token}) : super(key: key);
  @override
  _OrderManagerState createState() => _OrderManagerState();
}

class _OrderManagerState extends State<OrderManager> {
  var orders;
  String selectedVenue="";

  void onSelectedVenue(String id){

    setState(() {
      selectedVenue = id;
    });

  }
  
  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = BlocProvider.of<UserBloc>(context);
    var venues= userBloc.getVenues(widget.token);
     return Scaffold(
      appBar: AppBar(
        title: Text("Ordenes"),
      ),
      drawer: SideMenu(widget.token,(){}),
      body: Column(children: <Widget>[
      VenueDropdown(onSelectedVenue:onSelectedVenue , venues:venues),
      StreamBuilder(
      stream: Stream.periodic(Duration(seconds: 10))
          .asyncMap((i) => userBloc.getOrders(widget.token)), // i is null here (check periodic docs)
      builder: (context, snapshot) {
        if(snapshot.hasData){

        return  ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  padding: EdgeInsets.all(8),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index){
                    return
                      Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
             ListTile(
              leading: Icon(Icons.kitchen),
              title: Text(snapshot.data[index]["product_name"]),
              subtitle: Text(snapshot.data[index]["price"]),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                TextButton(
                  child:  Text('Aceptar'),
                  onPressed: () {/* ... */},
                ),
                 SizedBox(width: 8),
                TextButton(
                  child:  Text('Rechazar'),
                  onPressed: () {/* ... */},
                ),
                 SizedBox(width: 8),
              ],
            ),
          ],
        ),
      );
                  });}else{
                     return Center(child: CircularProgressIndicator());
                  }
                  } )],), // builder should also handle the case when data is not fetched yet     
    );
  }
}

class ListItem {
  String id;
  String name;

  ListItem(this.id, this.name);
}