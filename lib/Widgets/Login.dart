import 'dart:ui';

import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  final loginAction;
  final String loginError;

  const Login(this.loginAction, this.loginError);

  
  @override
  Widget build(BuildContext context) {
    Widget button = SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: RaisedButton(
           onPressed: () {
            loginAction();
          },
          color: Color.fromARGB(255,236, 187, 48),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: Padding(
            padding: EdgeInsets.only(
                top: 1 / 2, bottom: 1 / 2),
            child: Text(
              "Login",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w700),
            ),
          ),
        ),
      ),
    );


    Widget imagen =  Container(
      child: Stack(
        children:[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/logitopy.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5.0 , sigmaY: 5.0 ),
             
            ),
          ),
          Center(
            child: Column(
              children: [
                Image(image: AssetImage("assets/images/mimenulogo.png") ),
                button]
            )
          )
              ]
            ),
          );
    return imagen;
  }

 
}