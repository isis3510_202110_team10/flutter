import 'package:flutter/material.dart';

// Colors that we use in our app
const kPrimary = Color(0xFFF6C946);
const kSecondary = Color(0xFF141414);


const double kDefaultPadding = 20.0;
