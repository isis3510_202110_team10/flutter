import 'package:flutter/material.dart';

import 'Restaurant/ui/screens/availability_manager.dart';
import 'Restaurant/ui/screens/order_manager.dart';

  final List<Map<String,dynamic>> pages = [{"name":"sedes","index":0},{"name":"pedidos","index":1}];

class SideMenu extends StatelessWidget {

    final void Function() logOut;
    String token;
  SideMenu( this.token,this.logOut);

 
  @override
  Widget build(BuildContext context) {
    return  Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children:  <Widget>[
        DrawerHeader(
          decoration: BoxDecoration(
            color: Color.fromARGB(255,236, 187, 48),
          ),
          child: Container(
            decoration:
             BoxDecoration(image: DecorationImage(
               image: AssetImage('assets/images/compleat.svg')),),
          ),
        ),
        ListTile(
          leading: Icon(Icons.restaurant),
          title: Text('Dispoibilidades'),
          onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => AvailabilityManager(token:token)));
            },
        ),
        ListTile(
          leading: Icon(Icons.shopping_cart),
          title: Text('Pedidos'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => OrderManager(token:token)));
            },
        ),
         ListTile(
          leading: Icon(Icons.logout),
          title: Text('Salir'),
            onTap: (){logOut();},
        )
      ],
    ),
  );
  }
}