


import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:compleat/side_menu.dart';
import 'package:geolocator/geolocator.dart';

import "./Auth/variables.dart";
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'Restaurant/ui/screens/order_manager.dart';
import 'Restaurant/ui/screens/availability_manager.dart';
import 'User/ui/screens/log_in_screen.dart';
import 'get_location.dart';


final FlutterAppAuth appAuth = FlutterAppAuth();
final FlutterSecureStorage secureStorage = const FlutterSecureStorage();
class AppLayout extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _AppLayout();
  }
  
}

class _AppLayout extends State<AppLayout> {
    bool isBusy = false;
  bool isLoggedIn = false;
  String errorMessage;
    String token;
 String variable="sapo";
  int indexTap = 0;
  Position position;
  



   getCurrentLocation() async {
    final position = await determinePosition();
    setState(() {
      this.position=position;
      
    });
  }
  Widget renderScrens () {
    if(isLoggedIn){
       double distance = Geolocator.distanceBetween(37, -122, position.latitude, position.longitude);
        print("distancia:");
        if(distance>10000){
          return AvailabilityManager(token:token);
        }
        else{
          logoutAction();
        } 
    }
    return Login(loginAction, errorMessage);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return  Scaffold(
            
            body:isBusy
              ? Center(child: CircularProgressIndicator())
              : renderScrens(),
          );
  }

 

 Map<String, dynamic> parseIdToken(String idToken) {
  
    final parts = idToken.split(r'.');
    assert(parts.length == 3);

    return jsonDecode(
        utf8.decode(base64Url.decode(base64Url.normalize(parts[1]))));
  }

  Future<void> loginAction() async {
    setState(() {
      isBusy = true;
      errorMessage = '';
    });

    try {
      final AuthorizationTokenResponse result =
          await appAuth.authorizeAndExchangeCode(
        AuthorizationTokenRequest(
          AUTH0_CLIENT_ID,
          AUTH0_REDIRECT_URI,
          issuer: 'https://$AUTH0_DOMAIN',
          scopes: ['openid', 'profile', 'offline_access',"api"]
          // promptValues: ['login']
        ),
      );
      await secureStorage.write(
          key: 'refresh_token', value: result.refreshToken);
      setState(() {
        isBusy = false;
        isLoggedIn = true;
        token=result.idToken;
      });
  
    } catch (e, s) {
      print('login error: $e - stack: $s');

      setState(() {
        isBusy = false;
        isLoggedIn = false;
        errorMessage = e.toString();
      });
    }
    getCurrentLocation();

    
 
  }

  void logoutAction() async {
    await secureStorage.delete(key: 'refresh_token');
    setState(() {
      isLoggedIn = false;
      isBusy = false;
    });
  }

  @override
  void initState() {
    initAction();
    super.initState();
  }

  void initAction() async {
    final storedRefreshToken = await secureStorage.read(key: 'refresh_token');
    if (storedRefreshToken == null) return;

    setState(() {
      isBusy = true;
    });

    try {
      final response = await appAuth.token(TokenRequest(
        AUTH0_CLIENT_ID,
        AUTH0_REDIRECT_URI,
        issuer: AUTH0_ISSUER,
        refreshToken: storedRefreshToken,
      ));

      final idToken = response.idToken;
     

      secureStorage.write(key: 'refresh_token', value: response.refreshToken);

      setState(() {
        isBusy = false;
        isLoggedIn = true;
        token=idToken;
      });
    } catch (e, s) {
      print('error on refresh token: $e - stack: $s');
      logoutAction();
    }
  }
  

}